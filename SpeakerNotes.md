# Mikado Method

## 1. Intro

## 2. Xcode errors

Have you ever been in a situation like this?

You’re knee deep in some big code changes, everything is broken, and you’ve just learned that yes, Xcode _can_ happily complain about 5003 build errors.

Do you dread going to your daily standup, to report that “the error count is now under 3000, and  you hope to finish the job in the next two weeks”?

Or worse still, you’re asked to work on something else, and you think “oh man, do I really want to `git stash` _this mess_?”

## 3. Pull request

But it’s worse for your colleagues.
They see the mess you’re making, and they know it’ll be the biggest pull request they’ve ever seen in their lives.

_Zoom in_

There are so many files changed, that even GitHub has stopped counting.

## 4. Using the Mikado Method

The Mikado Method is here to save you, by
* Breaking down a large task into small deliverable chunks
* Keeping your colleagues happy
* So you can go *onward to great victory*

How does it work?

## 5. Start with your goal in mind

Start by thinking about your goal, that’s the thing you ultimately want to achieve:
* Maybe you have a `Double` value which is used everywhere, and you’d like to change it to a `Money` value type
* Maybe you want to remove a singleton from the app
* Or perhaps you want to move some code into a module, but there’s a load of spaghetti code which will make the task difficult

And here’s what you do:

## 6. Just make the change

Just make the change:
Change the Double value to be a Money value
Delete the `static let` that defines the singleton that you hate.
Just do it, and hit command B.

It’ll fail to build, of course, but that’s good - because you’ll learn what you need to change in the app before you can reach your ultimate goal.

So we’re gonna make a tree of dependent tasks.

## 7. A tree of tasks

Here’s our tree, with our goal at the top, highlighted in purple.
We’ve made our change, the code doesn’t build, and we’ve found three things that have broken because of that change
So those are the things we need to work on next.

And here’s the magic trick of the Mikado Method

## 8. Git Reset

`git reset --hard` wipes away all of your code changes. This is lost code, but not lost effort, because we’ve learned something by making that change
Let’s get back to the tree.

## 9. Back to the tree

We can now focus on the next task, highlighted in purple.
Just make that change, and hit command B and… oh no, that fails too.
But we’ve found two more tasks that we need to do.

Git reset again.

And we carry on like this, learning about the dependencies in our code, and building up this tree.

Until…

You’re at this dependency in the bottom left.
You’ve git reset, and you just make _this_ change,
… and… it works! Nothing more broke.

So here’s where you make your first pull request.
If you’re feeling very kind, you can say “this is pull request 1 of 12, and here’s what I’m changing next”

And then you carry on:
* git reset
* make the change
* if it passes, make a pull request
* if it fails, add another task to the tree

Eventually, all the sub-tasks will be done, they’ve all been merged into your app, one at a time, leaving just that end goal remaining, and one final pull request.

## Summary

So, in summary:
* Use the Mikado Method when you have a complicated messy job to do
* Build that tree of dependencies, draw it on paper
* `git reset` at every stage
* Make multiple PRs, as you work your way from the bottom of the tree to the top

Thank-you!



#iOSDevUK
