//
//  ContentView.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

struct ContentView: View {
	var body: some View {
		Presenter(deck: self.deck, showCamera: false)
	}
}

extension ContentView {
	var deck: Deck {
		Deck(title: "iOSDevUK 2023") {
			Intro.slide
			XcodeErrors.slide
			MassivePullRequest.slide
			UseTheMikadoMethod.slide
			StartWithYourGoal.slide
			JustMakeTheChange.slide
			TreeOfTasks.slide
			MagicTrick.slide
			TreeOfTasks.slide
			Summary.slide
		}
	}
}
#Preview {
    ContentView()
}
