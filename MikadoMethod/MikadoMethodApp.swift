//
//  MikadoMethodApp.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI

@main
struct MikadoMethodApp: App {
	var body: some Scene {
		WindowGroup {
			ContentView()
		}
	}
}
