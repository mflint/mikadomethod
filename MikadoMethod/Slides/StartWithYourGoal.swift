//
//  StartWithYourGoal.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum StartWithYourGoal {
	static let slide = Slide {
		Title("Start with your goal")

		Columns {
			Column {
				Bullets {
					Words("`let accountBalance: Double`      ➡️      `let accountBalance: Money`", markdown: true)
					Words("Remove a singleton?")
					Words("Extract code into a module")
				}
			}
		}
	}
}

#Preview {
	SlidePreviewView(slide: StartWithYourGoal.slide)
}

