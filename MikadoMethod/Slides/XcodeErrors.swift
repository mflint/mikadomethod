//
//  XcodeErrors.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum XcodeErrors {
	static let slide = Slide {
		Title("Xcode can be so picky at times")

		Columns {
			Column {
				RawView {
					Image("AllTheErrors")
						.resizable()
						.aspectRatio(contentMode: .fit)
						.frame(maxWidth: .infinity, maxHeight: .infinity)
				}
			}
		}
	}
}

#Preview {
	SlidePreviewView(slide: XcodeErrors.slide)
}
