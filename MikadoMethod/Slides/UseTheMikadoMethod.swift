//
//  UseTheMikadoMethod.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum UseTheMikadoMethod {
	static let slide = Slide {
		Title("Use the Mikado Method!")

		Columns {
			Column {
				Bullets {
					Words("Break down a large task into small deliverable chunks")
					Words("Keep your colleagues happy")
					Words("_Onward to Great Victory_", markdown: true)
				}
			}
		}
	}
}

#Preview {
	SlidePreviewView(slide: UseTheMikadoMethod.slide)
}
