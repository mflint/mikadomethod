//
//  Intro.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum Intro {
	static let slide = Slide {
		Title("The Mikado Method for refactoring code", subtitle: "Matthew Flint")
	}
}

struct SlidePreviewView: View {
	let slide: Slide

	var body: some View {
		Presenter(deck: Deck(title: "Preview") {
			self.slide
		}, showCamera: false)
	}
}

#Preview {
	SlidePreviewView(slide: Intro.slide)
}
