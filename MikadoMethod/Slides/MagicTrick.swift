//
//  MagicTrick.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum MagicTrick {
	static let slide = Slide {
		Title("Magic trick")

		Words("`git reset --hard`", markdown: true)
	}
}

#Preview {
	SlidePreviewView(slide: MagicTrick.slide)
}
