//
//  MassivePullRequest.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum MassivePullRequest {
	static let slide = Slide {
		Title("It's worse for your colleagues")

		Columns {
			Column {
				RawView {
					MassiveImage()
				}
			}
		}
	}
}

private struct MassiveImage: View {
	@State private var zoomed: Bool = false
	var scale: Double {
		zoomed ? 2.2 : 1.0
	}

	var body: some View {
		Color.black.opacity(0.01)
			.overlay {
				Image("MassivePullRequest")
					.resizable()
					.aspectRatio(contentMode: .fit)
					.scaleEffect(CGSize(width: self.scale, height: self.scale), anchor: .trailing)
			}
			.frame(maxWidth: .infinity, maxHeight: .infinity)
			.onTapGesture {
				withAnimation {
					self.zoomed.toggle()
				}
			}
	}
}

#Preview {
	SlidePreviewView(slide: MassivePullRequest.slide)
}
