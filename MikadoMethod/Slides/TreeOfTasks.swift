//
//  TreeOfTasks.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum TreeOfTasks {
	static let slide = Slide {
		Title("Make a tree of dependencies")

		Columns {
			Column {
				RawView {
					TaskTreeView()
				}
			}
		}
	}
}

private struct MikadoTask {
	let name: String
	let highlighted: Bool
	let state: TaskState

	init(name: String, highlighted: Bool = false, state: TaskState = .blank) {
		self.name = name
		self.highlighted = highlighted
		self.state = state
	}
}

enum TaskState {
	case blank
	case fail
	case success

	var systemImage: String? {
		switch self {
		case .blank: nil
		case .fail: "xmark"
		case .success: "checkmark"
		}
	}

	var color: Color {
		switch self {
		case .blank: .white
		case .fail: .red
		case .success: .green
		}
	}
}

private struct TaskTreeView: View {
	static let trees: [Tree<MikadoTask>] = [
		Tree(MikadoTask(name: "Goal", highlighted: true, state: .fail), children: [
			Tree(MikadoTask(name: "Sub-task 1")),
			Tree(MikadoTask(name: "Sub-task 2")),
			Tree(MikadoTask(name: "Sub-task 3")),
		]),

		Tree(MikadoTask(name: "Goal", state: .fail), children: [
			Tree(MikadoTask(name: "Sub-task 1", highlighted: true)),
			Tree(MikadoTask(name: "Sub-task 2")),
			Tree(MikadoTask(name: "Sub-task 3")),
		]),

		Tree(MikadoTask(name: "Goal", state: .fail), children: [
			Tree(MikadoTask(name: "Sub-task 1", highlighted: true, state: .fail), children: [
				Tree(MikadoTask(name: "Sub-task 4")),
				Tree(MikadoTask(name: "Sub-task 5")),
			]),
			Tree(MikadoTask(name: "Sub-task 2")),
			Tree(MikadoTask(name: "Sub-task 3")),
		]),

		Tree(MikadoTask(name: "Goal", state: .fail), children: [
			Tree(MikadoTask(name: "Sub-task 1", state: .fail), children: [
				Tree(MikadoTask(name: "Sub-task 4", state: .fail), children: [
					Tree(MikadoTask(name: "Sub-task 6", highlighted: true))
				]),
				Tree(MikadoTask(name: "Sub-task 5")),
			]),
			Tree(MikadoTask(name: "Sub-task 2", state: .fail), children: [
				Tree(MikadoTask(name: "Sub-task 7")),
			]),
			Tree(MikadoTask(name: "Sub-task 3", state: .fail), children: [
				Tree(MikadoTask(name: "Sub-task 8")),
				Tree(MikadoTask(name: "Sub-task 9")),
				Tree(MikadoTask(name: "Sub-task 10", state: .fail), children: [
					Tree(MikadoTask(name: "Sub-task 11"))
				]),
			]),
		]),

		Tree(MikadoTask(name: "Goal", state: .fail), children: [
			Tree(MikadoTask(name: "Sub-task 1", state: .fail), children: [
				Tree(MikadoTask(name: "Sub-task 4", state: .fail), children: [
					Tree(MikadoTask(name: "Sub-task 6", highlighted: true, state: .success))
				]),
				Tree(MikadoTask(name: "Sub-task 5")),
			]),
			Tree(MikadoTask(name: "Sub-task 2", state: .fail), children: [
				Tree(MikadoTask(name: "Sub-task 7")),
			]),
			Tree(MikadoTask(name: "Sub-task 3", state: .fail), children: [
				Tree(MikadoTask(name: "Sub-task 8")),
				Tree(MikadoTask(name: "Sub-task 9")),
				Tree(MikadoTask(name: "Sub-task 10", state: .fail), children: [
					Tree(MikadoTask(name: "Sub-task 11"))
				]),
			]),
		]),

		Tree(MikadoTask(name: "Goal", highlighted: true, state: .fail), children: [
			Tree(MikadoTask(name: "Sub-task 1", state: .success), children: [
				Tree(MikadoTask(name: "Sub-task 4", state: .success), children: [
					Tree(MikadoTask(name: "Sub-task 6", state: .success))
				]),
				Tree(MikadoTask(name: "Sub-task 5", state: .success)),
			]),
			Tree(MikadoTask(name: "Sub-task 2", state: .success), children: [
				Tree(MikadoTask(name: "Sub-task 7", state: .success)),
			]),
			Tree(MikadoTask(name: "Sub-task 3", state: .success), children: [
				Tree(MikadoTask(name: "Sub-task 8", state: .success)),
				Tree(MikadoTask(name: "Sub-task 9", state: .success)),
				Tree(MikadoTask(name: "Sub-task 10", state: .success), children: [
					Tree(MikadoTask(name: "Sub-task 11", state: .success))
				]),
			]),
		]),

		Tree(MikadoTask(name: "Goal", state: .success), children: [
			Tree(MikadoTask(name: "Sub-task 1", state: .success), children: [
				Tree(MikadoTask(name: "Sub-task 4", state: .success), children: [
					Tree(MikadoTask(name: "Sub-task 6", state: .success))
				]),
				Tree(MikadoTask(name: "Sub-task 5", state: .success)),
			]),
			Tree(MikadoTask(name: "Sub-task 2", state: .success), children: [
				Tree(MikadoTask(name: "Sub-task 7", state: .success)),
			]),
			Tree(MikadoTask(name: "Sub-task 3", state: .success), children: [
				Tree(MikadoTask(name: "Sub-task 8", state: .success)),
				Tree(MikadoTask(name: "Sub-task 9", state: .success)),
				Tree(MikadoTask(name: "Sub-task 10", state: .success), children: [
					Tree(MikadoTask(name: "Sub-task 11", state: .success))
				]),
			]),
		]),
	]

	@State var treeIndex = 0
	var binaryTree: Tree<MikadoTask> { Self.trees[self.treeIndex] }

	var unique: Tree<Unique<MikadoTask>> {
		self.binaryTree.map(Unique.init)
	}

	var body: some View {
		Color.black.opacity(0.01)
			.overlay {
				Diagram(tree: unique, node: { task in
					TaskTreeNode(task: task.value)
				})
			}
		.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
		.onTapGesture {
			withAnimation {
				if self.treeIndex < Self.trees.count - 1 {
					self.treeIndex += 1
				}
			}
		}
	}
}

private struct TaskTreeNode: View {
	let task: MikadoTask

	var body: some View {
		Text(task.name)
			.font(.system(size: 35))
			.foregroundStyle(Color.white)
			.padding(16)
			.background {
				RoundedRectangle(cornerRadius: 12)
					.strokeBorder(task.highlighted ? .purple : .white, lineWidth: 6)
			}
			.background {
				Color.black
					.clipShape(RoundedRectangle(cornerRadius: 12))
			}
			.padding()
			.overlay {
				self.task.state.systemImage.map { systemImage in
					Image(systemName: systemImage)
						.fontWeight(.black)
						.foregroundStyle(self.task.state.color)
						.padding()
						.background {
							Circle()
								.strokeBorder(self.task.state.color, lineWidth: 6)
								.background {
									Color.black
										.clipShape(Circle())
								}
						}
						.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottomTrailing)
				}
			}
	}
}

#Preview {
	SlidePreviewView(slide: TreeOfTasks.slide)
}
