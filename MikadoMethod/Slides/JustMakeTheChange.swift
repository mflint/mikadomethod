//
//  JustMakeTheChange.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI
import Splash

enum JustMakeTheChange {
	static let slide = Slide {
		Title("Just make the change")

		Code {
   """
 let accountBalance: Money // Double


 // static let instance = MyClass()
 """
		}

		Words("... and then build. ⌘B", font: Font.system(size: 80))
	}
}

#Preview {
	SlidePreviewView(slide: JustMakeTheChange.slide)
}

