//
//  Summary.swift
//  MikadoMethod
//
//  Created by Matthew Flint on 2023-09-06.
//

import SwiftUI
import DeckUI

enum Summary {
	static let slide = Slide {
		Title("Summary")

		Columns {
			Column {
				Bullets {
					Words("Use the Mikado Method when you have a messy refactoring job to do")
					Words("Build a tree of dependencies")
					Words("Don't be afraid to `git reset --hard`", markdown: true)
					Words("Make multiple PRs")
				}
			}
		}
		Words("Matthew Flint")
		Words("🦣 `@matthew@mastodon.me.uk`", markdown: true)
	}
}

#Preview {
	SlidePreviewView(slide: Summary.slide)
}
