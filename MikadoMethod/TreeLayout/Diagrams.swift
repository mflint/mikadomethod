//
//  Diagrams.swift
//  DiagramsSample
//
//  Created by Chris Eidhof on 16.12.19.
//  Copyright © 2019 objc.io. All rights reserved.
//
import SwiftUI

/*
 https://gist.github.com/chriseidhof/b5f91ca23f7b98307c066218d4b119ff#file-diagrams-swift
 */

/// A simple Tree datastructure that holds nodes with `A` as the value.
struct Tree<A> {
	var value: A
	var children: [Tree<A>] = []
	init(_ value: A, children: [Tree<A>] = []) {
		self.value = value
		self.children = children
	}
}

extension Tree {
	func map<B>(_ transform: (A) -> B) -> Tree<B> {
		return Tree<B>(transform(value), children: children.map({ $0.map(transform) }))
	}
}

class Unique<A>: Identifiable {
	let value: A
	init(_ value: A) { self.value = value }
}

struct CollectDict<Key: Hashable, Value>: PreferenceKey {
	static var defaultValue: [Key:Value] { [:] }
	static func reduce(value: inout [Key:Value], nextValue: () -> [Key:Value]) {
		value.merge(nextValue(), uniquingKeysWith: { $1 })
	}
}

/// This is needed to use `CGPoint` as animatable data
extension CGPoint: VectorArithmetic {
	public static func -= (lhs: inout CGPoint, rhs: CGPoint) {
		lhs = lhs - rhs
	}

	public static func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
		return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
	}

	public static func += (lhs: inout CGPoint, rhs: CGPoint) {
		lhs = lhs + rhs
	}

	public mutating func scale(by rhs: Double) {
		x *= CGFloat(rhs)
		y *= CGFloat(rhs)
	}

	public static func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
		return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
	}

	public var magnitudeSquared: Double { return Double(x*x + y*y) }
}

/// Draws an edge from `from` to `to`
struct Line: Shape {
	var from: CGPoint
	var to: CGPoint
	var animatableData: AnimatablePair<CGPoint, CGPoint> {
		get { AnimatablePair(from, to) }
		set {
			from = newValue.first
			to = newValue.second
		}
	}

	func path(in rect: CGRect) -> Path {
		Path { p in
			p.move(to: self.from)
			p.addLine(to: self.to)
		}
	}
}

/// A simple Diagram. It's not very performant yet, but works great for smallish trees.
struct Diagram<A: Identifiable, V: View>: View {
	let tree: Tree<A>
	let node: (A) -> V

	typealias Key = CollectDict<A.ID, Anchor<CGPoint>>

	var body: some View {
		VStack(alignment: .center, spacing: 30) {
			node(tree.value)
			   .anchorPreference(key: Key.self, value: .center, transform: {
				   [self.tree.value.id: $0]
			   })
			HStack(alignment: .bottom, spacing: 30) {
				ForEach(tree.children, id: \.value.id, content: { child in
					Diagram(tree: child, node: self.node)
				})
			}
			Spacer()
		}.backgroundPreferenceValue(Key.self, { (centers: [A.ID: Anchor<CGPoint>]) in
			GeometryReader { proxy in
				ForEach(self.tree.children, id: \.value.id, content: {
				 child in
					Line(
						from: proxy[centers[self.tree.value.id]!],
						to: proxy[centers[child.value.id]!])
					.stroke()
				})
			}
		})
	}
}
