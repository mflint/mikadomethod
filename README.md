# About this repo

This is the code for a lightning talk that I presented at [iOSDevUK 2023](https://www.iosdevuk.com) on 6th September 2023.

The presentation is a macOS app, using the [DeckUI DSL](https://github.com/joshdholtz/DeckUI) by [Josh Holtz](http://www.joshholtz.com).

# About the talk

The subject of the talk is the _Mikado Method_ for refactoring difficult code with less pain. Any errors or diversions from the original Mikado Method any wholly my fault.

## Speaker notes

Speaker notes have been braindumped into a separate file. See [Speaker Notes](./SpeakerNotes.md).

## About the images

The "5003 build failures" image is mine, and may be freely re-used. It's easy to do this on your own project by find/replacing a common keyword (like `private`) with some nonsense, and then enabling the "Continue building after errors" option in Xcode settings.

The "massive PR" screenshot is taken from this [heroic pull request in the Emacs repo](https://github.com/emacs-mirror/emacs/pull/6).

## How to drive this talk

* use the right and left arrow keys to move backwards and forwards
* the "massive PR" image is tappable to zoom in
* the "tree" view is tappable to move to the next state. I only started tapping on this view after returning to the view after the `git reset` screen

# Inspiration

* Daniel Brolund and Ola Ellnestam, creators of [The Mikado Method](https://mikadomethod.info)
* [Kevin Rutherford](https://www.kevinrutherford.info/) who introduced this idea to me

# License

My small contributions to this repo are freely usable under MIT licence. That'll be just the presentation content and speaker notes.

This repo contains code from [Chris Eidof](https://twitter.com/chriseidhof) at [objc.io](https://www.objc.io/blog/2019/12/16/drawing-trees/) for drawing trees, with some adaptation from me. Bugs will be mine, not his.

